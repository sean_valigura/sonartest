package leo.pp;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        boolean b = false;
        System.out.println( "Hello World!" );
        if (b) {
            // reachability is fixed at the static code
            if (b) System.out.println(" this will either print everything below or not at all");
            System.out.println("b is " + b);
        }
    }
}
